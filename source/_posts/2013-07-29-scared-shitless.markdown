---
layout: post
title: "Scared Shitless"
date: 2013-07-29 08:51
comments: true
categories: [Talks, Fear]
---

I just saw this awesome talk by [Merlin Mann](http://www.43folders.com/) ([@hotdogsladies](https://twitter.com/hotdogsladies)) titled **Scared Shitless**.

<iframe width="560" height="315" src="//www.youtube.com/embed/Lk0hSeQ5s_k" frameborder="0" allowfullscreen></iframe>

If there's something you want to do but you've been too scared to do it maybe you should watch this. Get a little courage. Get some gumption! And do the damn thing! ROOOOOAAAAR!
