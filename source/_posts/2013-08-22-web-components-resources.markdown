---
layout: post
title: "Web Components Resources"
date: 2013-08-22 17:47
comments: true
categories: [HTML5, HTML Imports, Shadow DOM, Custom Elements, Polymer, Template, Web Components]
---

Eric Bidelman ([@ebidel](http://twitter.com/ebidel)) put together [a great list of Web Components Resources](https://gist.github.com/ebidel/6314025). It's nice to have it all in one place because it sure can get confusing hoping around from spec to article to slideshow, etc. If you have more resources make sure to do a PR and add them to the list!